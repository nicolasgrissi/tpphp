<?=

$pwd = "kangourou";
$pwd_session = $pwd;

// On démarre la session AVANT d'écrire du code HTML
session_start();

// On s'amuse à créer quelques variables de session dans $_SESSION
$_SESSION['pwd_session'] = $pwd_session;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Successfull access</title>
    <link rel="stylesheet" href="app.css">
</head>
<body>

<div class="wrapper">

    <h1>Successfull access</h1>
    <h3>Central server keys :</h3>

    <?=

    $input_pwd = htmlspecialchars($_POST['pwd']);
    $pwd = "kangourou";
    $pwd_session = $pwd;
    $server_keys = "<p>5565-565 <br> 5554-887 <br> 7875-565</p>";

    if(isset($input_pwd) AND !empty($input_pwd) AND $input_pwd == $pwd)
    {
        echo "<h4>Access codes :</h4>";
        echo $server_keys;

    }
    else
    {
        echo "<h3>Access denied</h3>";
        echo "<a href='index_controler.php' >Try again</a>";
    }
?>

    <p><em>All the follwings informations are confidentials</em></p>

    <h3>Tu peux aller voir ta <a href="session.php">session</a></h3>

</div>

</body>
</html>