<?php

require("./pdo/pdo.php");
require("./classes/admin.class.php");


$oAdmin = new admin($bdd); // oBlog object init
session_start();
$rqt_postAdmin = $oAdmin->getAdminPosts();// permet de récupérer la requete
$oAdmin->AddAdminPost();
$oAdmin->ModAdminPostTitle();
$oAdmin->ModAdminPostContent();
$oAdmin->ModAdminPostIMG();
$oAdmin->DelAdminPost();

require("./vue/admin_vue.php");// récupère la vue