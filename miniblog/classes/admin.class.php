<?php

class admin
{
    private $bdd;

    /**
     * admin constructor.
     * INIT PDO OBJECT
     * @param $bdd
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * GET ALL THE POSTS FOR ADMIN PANEL
     *
     * @return object return sql request
     */
    public function GetAdminPosts()
    {
        $rqt_postAdmin = $this->bdd->query("SELECT id,titre,contenu,img, DATE_FORMAT(date_creation, '%d/%m/%Y <br>à %Hh%i') AS date_creation FROM articles  ORDER BY id DESC ");
        return $rqt_postAdmin;
    }


    /**
     * ADD NEW ARTICLE
     */
    public function AddAdminPost()
    {
        if (isset($_POST["addpost_title"]) AND isset($_POST["addpost_content"])) {
            $add_title = strip_tags($_POST['addpost_title']);
            $add_content = strip_tags($_POST['addpost_content']);
            $add_img = strip_tags($_POST['addpost_img']);

            $rqt_addpost = $this->bdd->prepare("INSERT INTO articles (titre,contenu,img,date_creation) VALUES (:addtitle , :addcontent, :img , NOW())");
            $rqt_addpost->execute(array(
                'addtitle' => $add_title,
                'addcontent' => $add_content,
                'img' => $add_img
                /*NOW() est déja inseré dans la preparation SQL */
            ));
            $rqt_addpost->closeCursor();
            header('location:admin.php');
        }
    }

    /**
     * MODIFY ARTICLE TITLE
     */
    public function ModAdminPostTitle()
    {
        if (isset($_POST['mod_titre_id']) AND isset($_POST['mod_titre'])) {
            $rqt_mod = $this->bdd->prepare("UPDATE articles SET titre = :mod_titre WHERE id = :id_mod");
            $rqt_mod->execute(array(
                mod_titre => $_POST['mod_titre'],
                id_mod => $_POST['mod_titre_id']
            ));
            $rqt_mod->closeCursor();
            header('location:admin.php');
        }
    }

    /**
     * MODIFY ARTICLE CONTENT
     */
    public function ModAdminPostContent()
    {
        if (isset($_POST['mod_cont']) AND isset($_POST['mod_cont_id'])) {
            $rqt_mod = $this->bdd->prepare("UPDATE articles SET contenu = :mod_cont WHERE id = :id_mod");
            $rqt_mod->execute(array(
                mod_cont => htmlspecialchars(nl2br($_POST['mod_cont'])),
                id_mod => $_POST['mod_cont_id']
            ));
            header('location:admin.php');
            $rqt_mod->closeCursor();
        }
    }

    /**
     * MODIFY ARTICLE IMG URL
     */
    public function ModAdminPostIMG()
    {
        if (isset($_POST['mod_img']) AND isset($_POST['mod_img_id'])) {
            $rqt_mod = $this->bdd->prepare("UPDATE articles SET img = :mod_img WHERE id = :id_mod");
            $rqt_mod->execute(array(
                mod_img => htmlspecialchars($_POST['mod_img']),
                id_mod => $_POST['mod_img_id']
            ));
            $rqt_mod->closeCursor();
            header('location:admin.php');
        }
    }

    /**
     * DELETE SELECTED POST
     */
    public function DelAdminPost()
    {
        if (isset($_POST['admin_delpost'])) {
            $rqt_del = $this->bdd->prepare("DELETE  FROM articles WHERE id = :id_del");
            $rqt_del->execute(array(id_del => $_POST['admin_delpost']));
            header('location:admin.php');
            $rqt_del->closeCursor();
        }
    }

}