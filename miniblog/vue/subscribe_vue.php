<?php $title = "Inscription"; ?>
<?php ob_start(); //enclenche la temporisation de sortie ?>

<div class="wrapper">
	<div class="articles">
        <span>
        <a href="index.php" class=" btn btn-back "> Revenir aux articles</a>
        </span>
		<div class="subscribe-box">
			<h2 class="title-box">Inscription</h2>
			<p><em class="subtitle-box info">Pour vous inscrire veuillez créer un login et un mots de passe</em></p><?php
			 if(isset($_SESSION['message'])){ echo "<small class='alert alert-1 info'>".$_SESSION['message']['exist']."</small>";}
			?>
			<form action="subscribe.php" method="post">
				<label for="login">Login</label> <br><input type="text" name="login" placeholder="Votre login" required>
				<br>
				<label for="login">Mot de passe</label>
				<br><input type="text" name="password" placeholder="votre mot de passe" required>
				<br>
				<button class="btn btn-subscribe" type="submit">S'inscrire</button>
			</form>
		</div>
	</div>
</div>
<?php $content = ob_get_clean(); // lit le contenu courant du tampon de sortie puis l'efface ?>
<?php require("./template/template.php"); ?>



