<?php $title = "Connexion"; ?>
<?php ob_start(); //enclenche la temporisation de sortie ?>
<div class="wrapper">
    <div class="articles">
        <span>
        <a href="index.php" class="btn btn-back ">Revenir aux articles</a>
        </span>
        <div class="login_box">
	        <h2 class="title-box">Acces à l'administration</h2>
            <p><em class="subtitle-box info">Pour acceder à l'administration veuillez entrer votre login et mots de passe</em>
                <br><br>
            <small class="alert-1 info"><i class="fas fa-info-circle"></i> Si vous n'êtes pas déjà enregistré, inscrivez vous <u><a href="subscribe.php">ici</a></u> </small>
                <?php
                if(isset($_SESSION['message'])){ echo "<br><small class='alert alert-2 info'><i class=\"fas fa-minus-circle\"></i> ".$_SESSION['message']['denied']."</small>";}
                ?>
            </p>
            <form action="login.php" method="POST">
                <label for="login">Login</label> <br><input type="text" name="login" required>
                <br>
                <label for="password">Mot de passe</label> <br><input type="password" name="password" required>
                <br>
                <span><button class="btn" type="submit">Se connecter</button> <a href="subscribe.php" class="btn btn-subscribe" >S'inscrire</a></span>
            </form>

        </div>
    </div>
</div>
<?php $content = ob_get_clean(); // lit le contenu courant du tampon de sortie puis l'efface ?>
<?php require("./template/template.php"); ?>

