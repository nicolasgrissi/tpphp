<?php $title = "Administration"; ?>
<?php ob_start(); //enclenche la temporisation de sortie ?>

<div class="wrapper">
	<div class="articles">
		<span><a href="/TPPHP/miniblog/" class=" btn btn-back ">Revenir aux articles</a> </span>

		<span>
			<?php if (isset($_SESSION['auth'])) {
                echo "<span class='badge badge-connected'>" . $_SESSION['auth']['name'] . " connecté</span>";
            } ?>
			<form action="/TPPHP/miniblog/login.php" method="GET">
                <input type="text" hidden name="session" value="0">
                <button class="btn btn-back" type="submit">Déconnexion</button>
            </form>
        </span>
		<table class="admin-table">
			<tbody>
			<tr class="admin-table-headrow">
				<th></th>
				<th>Titre</th>
				<th>Contenu</th>
				<th>Img (URL)</th>
				<th>Date</th>
				<th></th>
			</tr>
            <?php
            while ($post = $rqt_postAdmin->fetch()) {
                $id = (int)$post['id'];

                // if IMG is not defined
                if (empty($post['img'])) {
                    $post['img'] = "<em>no image</em>";
                } else {
                    $post['img'] = strip_tags(substr($post['img'], 0, 30)) . " ...";
                }

                echo " <tr class='admin-table-row'>
               <td class='table-see'><a  href='post.php?page=" . $post['id'] . "'><i class='far fa-eye'></i> </a></td>
               <td class='table-title'>" . strip_tags($post['titre']) . "
                
               <table width='100%'><tr>
               <td><form action='admin.php' method='post'><input  class='input-title input' placeholder='Le nouveau titre' type='text'   name='mod_titre'> <input name='mod_titre_id' type='hidden'  value='" . $id . "'> <br> <button class='table-btn' type='submit'>Modifier <i class=\"fas fa-check\"></i></button></form></td>
               </tr></table>
               </td>
               
               <td class='table-content'>" . strip_tags(substr($post['contenu'], 0, 100)) . "... 
               <table width='100%'><tr>
               <td><form action='admin.php' method='post'><textarea  class='input-cont input'  placeholder='Le nouvel article'  name='mod_cont'></textarea> <input name='mod_cont_id' type='hidden'  value='" . $id . "'> <br> <button class='table-btn' type='submit'>Modifier <i class=\"fas fa-check\"></i></button></form></td>
               </tr></table>
               </td>
                
               <td class='table-img'>" . $post['img'] . "
               <table width='100%'><tr>
               <td><form action='admin.php' method='post'><input class='input-img input'   type='text' placeholder='La nouvelle image'   name='mod_img'> <input name='mod_img_id' type='hidden'  value='" . $id . "'> <br> <button class='table-btn' type='submit'>Modifier <i class=\"fas fa-check\"></i></button></form></td>
               </tr></table>
               </td>
               
               <td class='table-date'>" . $post['date_creation'] . "</td>
               
               <td class='table-suppr' align='center'><form action='admin.php' method='post'>
               <button class='table-btn table-btn-suppr' name='admin_delpost' value='" . $id . "' type='submit'><i class='fas fa-trash-alt'></i></button>
				</form></td>
                </tr>";
            }
            $rqt_postAdmin->closeCursor();
            ?>
			</tbody>
		</table>

		<div class="form">
			<h4 class="commentaire-titre commentaire-form">Ajouter un article</h4>
			<form action="admin.php" method="post">
				<label for="addpost_title">Titre de
				                           l'article </label><br><input type="text" name="addpost_title" id="" required>
				<br>
				<label for="addpost_content">Article </label><br><textarea name="addpost_content" id="" cols="50" rows="6" required></textarea>
				<br>
				<label for="addpost_img">URL de la photo de
				                         l'article </label><br><input type="text" name="addpost_img" id="">
				<br>
				<br>
				<button class="btn btn-new" type="submit">Ajouter article</button>
			</form>
		</div>
	</div>
</div>

<?php $content = ob_get_clean(); // lit le contenu courant du tampon de sortie puis l'efface ?>

<?php require("./template/template.php"); ?>
