<?php $title="Actualités"; ?>

<?php ob_start(); //enclenche la temporisation de sortie ?>

<div class="wrapper">
	<div class="articles">
		<span class="btn btn-admin"><a href="./login.php">Administration</a></span>
		<br><br><?php

        while ($post = $req_home->fetch()) {

            // on vérifie que l'entree de l'URL de l'image est définie
            if ($post['img']) {
                $img = $post['img'];
            } else {
                $img = "./img/f1f1f1.png";
            };

            $id = (int)$post['id'];
            $post_title = $post['titre'];
            $post_date = $post['date_creation']; ?>


			<div class='article-box'>
			<div class='article-img' style='width : 200px ; height: 150px; overflow: hidden'>
				<a href='./post.php?page=<?= $id ?>'>
					<img height='100%' src='<?= $img ?>' alt=\"\">
				</a>
			</div>

			<div class='article-box-contenu'>
				<a href='./post.php?page=<?= $id ?>'>
					<h3 class='articles-title articles-title-home'><?= htmlspecialchars($post_title) ?></h3>
				</a>
				<small class='articles-date info'>&#9719; Ecrit le <?= $post_date ?></small>
			</div>
			</div><?php
        }
        $req_home->closeCursor(); ?>
		<br>
		<div class="pagination"><span><?= $oBlog->getPagination() ?></span></div>
		<br>
		<span class="btn btn-admin"><a href="./login.php">Administration</a></span>
	</div>
</div>

<?php $content = ob_get_clean(); // lit le contenu courant du tampon de sortie puis l'efface ?>


<?php require "./template/template.php"; ?>
